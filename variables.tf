variable "db_name" {
  description = "Unique name to assign to RDS instance"
}

variable "db_username" {
  description = "RDS root username"
  default = ["dbadm", "rdsadm"]
}

variable "db_password" {
  description = "RDS root user password"
  sensitive   = true
}
